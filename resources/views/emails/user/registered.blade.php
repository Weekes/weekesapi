@component('mail::message')
# Hi {{ $user->name }},

An account has been registered for you on the Weekes.io platform. You're now able to manage your project, track progress, access your staging site and so much more.

You'll need to click the link below to login and set your password. This link is only active for 24 hours.

<!-- todo Add Token Login with Password Reset -->
@component('mail::button', ['url' => config('app.url')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
