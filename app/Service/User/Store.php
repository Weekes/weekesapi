<?php

namespace App\Service\User;

use App\Model\User;
use Illuminate\Support\Facades\Hash;

class Store
{
    /**
     * Store user data.
     *
     * @param array $data
     * @return User
     */
    public function __invoke(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => $data['role'],
        ]);
    }
}
