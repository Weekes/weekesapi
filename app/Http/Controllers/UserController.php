<?php

namespace App\Http\Controllers;

use App\Mail\UserRegistered;
use App\Http\User\Requests\Store as StoreRequest;
use App\Model\User;
use App\Service\User\Store;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Create a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return User::paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param Store $store
     * @return JsonResponse
     */
    public function store(StoreRequest $request, Store $store)
    {
        $validated = $request->validated();

        $store($validated);

        # todo Defer this to an event so we're not holding up the process waiting for this to complete.
        # todo Complete Mail Template, Token Login & Password Change functionality.
        # Mail::to($user)->send(new UserRegistered($user));

        return response()->json(['message' => 'Account Created']);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function show(User $user)
    {
        return response(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
