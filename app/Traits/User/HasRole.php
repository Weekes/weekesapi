<?php

namespace App\Traits\User;

trait HasRole
{
    /**
     * Determine if the current user is an Administrator.
     *
     * @return bool
     */
    public function isAdministrator() {
        return $this->role === 'administrator';
    }
}
