<?php

namespace App\Console\Commands;

use App\Model\Delivery;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AdjustPriorities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adjust:priorities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adjust the delivery priorities based on their current priority & updated date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deliveries = Delivery::where([
            'status' => 'pending'
        ])->get();

        # Every 48 hours the priority of Low & Normal Deliveries increases.
        $period = Carbon::now()->subHours(48);

        foreach ($deliveries as $delivery)
        {
            # Check against updated_at so that we can account for any upgrades / downgrades in priority
            if ($delivery->priority === 'normal' && $delivery->updated_at <= $period) {
                $delivery->update([
                    'priority' => 'high'
                ]);
            }

            # Check against updated_at so that we can account for any upgrades / downgrades in priority
            if ($delivery->priority === 'low' && $delivery->updated_at <= $period) {
                $delivery->update([
                    'priority' => 'normal'
                ]);
            }
        }

        $this->info('Adjusted all Delivery priorities.');
    }
}
