<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'administrator',
            'client',
        ];

        User::truncate();

        # Generate specific Users with the specific roles
        foreach($roles as $role) {
            factory(User::class)->create([
                'role' => $role,
                'email' => $role . '@weekes.io',
            ]);
        }

        factory(User::class, 5)->create();
    }
}
