<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * List of all the seeders we currently use
     *
     * @var array
     */
    protected $seeders = [
        UserSeeder::class,
        TicketSeeder::class,
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        foreach ($this->seeders as $seeder)
        {
            $this->call($seeder);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
